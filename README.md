# Workshop DevSecOps

Repositório do laboratório do workshop **DevSecOps** ministrado por [Samuel Gonçalves](https://beacons.ai/sgoncalves).

## Sobre o Instrutor: Samuel Gonçalves

- 🇧🇷 Tech Lead na 4Linux
- Consultor em **Segurança Ofensiva** e **DevSecOps**
- Mais de 10 anos de experiência no mundo tech 💻
- Educou mais de *"5k++"* alunos globalmente 🌎
- Músico, Contista, YouTuber, Podcaster
- Entusiasta de Software Livre 💙

🔗 [Contato](https://beacons.ai/sgoncalves)

## Ementa do Workshop

### Aula 1: Fundamentos do Desenvolvimento Seguro

1. **Introdução ao Desenvolvimento Seguro**
   - Apresentação e objetivos
   - Relevância da segurança no desenvolvimento atual

2. **DevSecOps e sua História**
   - Evolução do DevSecOps
   - Integração da segurança no ciclo de desenvolvimento

3. **Análise Estática de Código**
   - Segurança de software
   - Análise de código estática
   - Uso do HorusecCLI para análises locais

4. **Preparando o Ambiente**
   - Montagem do laboratório com Horusec
   - Configuração inicial
   - Instalação do Horusec via Ansible

### Aula 2: Implementação e Melhores Práticas

1. **Avançando com o Horusec**
   - Personalização e adição de ferramentas
   - Configuração avançada

2. **Integração com CI/CD**
   - Fundamentos de CI/CD
   - Pipelines com Horusec
   - Automação de testes de segurança

3. **Análise de Vulnerabilidades**
   - Categorias analisadas pelo Horusec
   - Interpretação dos resultados
   - Tratamento de vulnerabilidades

4. **Melhores Práticas em Desenvolvimento Seguro**
   - Codificação segura
   - Uso de bibliotecas e frameworks confiáveis
   - Prevenção de vulnerabilidades comuns

## Configuração do Laboratório

### Pré-requisitos

- [Git](https://git-scm.com/downloads)
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
- [Vagrant](https://www.vagrantup.com/downloads)

> **Dicas:** 
> - Usuários Windows: Utilize o Powershell para instalação.
> - Usuários MAC OS: Se possível, utilize o gerenciador **brew**.

### Ambiente

O laboratório é baseado no [Vagrant](https://www.vagrantup.com/). Consulte o [Vagrantfile](./Vagrantfile) para detalhes das máquinas virtuais.

### Inicialização

1. Clone o repositório:
   ```bash
   git clone https://gitlab.com/o_sgoncalves/lab-workshop-devsecops
   cd lab-workshop-devsecops/
   ```

2. Inicie o laboratório:
   ```bash
   vagrant up
   ```

> **Nota:** A configuração pode levar algum tempo, dependendo da sua conexão e recursos do computador.

### Comandos Básicos do Vagrant

- `vagrant up`: Inicia as VMs
- `vagrant ssh <vm>`: Acessa a VM
- `vagrant reload <vm>`: Reinicia a VM
- `vagrant halt`: Desliga as VMs

📖 [Documentação Completa do Vagrant](https://www.vagrantup.com/docs)

## Configuração Horusec

Para começar a configurar o Horusec vamos acessar a máquina `devsecops` com o comando:
```shell=
vagrant ssh devsecops
```

Eleve seus privilégios aos do usuário `root` com o comando:
```shell=
sudo -i
```

Acesse o diretório `/opt/horusec` e realize a instalação com os comandos:
```shell=
cd /opt/horusec
make install
```

O provisionamento irá levar alguns minutos, é normal.

Após o provisionamento, acesse a interface web na URL: [http://192.168.56.60:8043/auth](http://192.168.56.60:8043/auth)

As credenciais padrão são:
```text=
email: dev@example.com
password: Devpass0*
```

#### Criar chave de API para projeto

1. Na tela inicial clique em "Add Workspace"
2. Adicione um Nome e uma Descrição
3. Após criar o workspace, selecione-o
4. Clique em "Add Repository"
5. Dê um nome e Descrição
6. Após criar o repositório, clique em "Overview"
7. Clique em "Tokens"
8. Clique em "Add token"
9. Na descrição, insira: "coffe-shop"
10. Copie o token gerado e insira no GitlabCI

## Repositórios Relacionados

- [Aplicação Principal](https://gitlab.com/o_sgoncalves/juice-shop-teste)
- [Loja de Cafés](https://github.com/thaycafe/coffee-shop)